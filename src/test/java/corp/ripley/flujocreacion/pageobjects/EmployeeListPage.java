package corp.ripley.flujocreacion.pageobjects;

import corp.ripley.flujocreacion.utils.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import static corp.ripley.flujocreacion.utils.MetodosGenericos.waitForElementToBeVisible;
import static corp.ripley.flujocreacion.utils.MetodosGenericos.waitForElementToBeVisibleAndClickable;


public class EmployeeListPage extends BasePage  {

    @FindBy(how = How.XPATH, using ="//*[@id=\"empsearch_employee_name_empName\"]")
    private WebElement buscarempleado;

    @FindBy(how = How.XPATH, using ="//*[@id=\"menu_pim_viewEmployeeList\"]")
    private WebElement menuemployee;

    @FindBy(how = How.CSS, using ="personalDetails")
    private WebElement contenedordetalles;

    @FindBy(how = How.XPATH, using ="//*[@id=\"sidenav\"]/li[1]/a")
    private WebElement btnpersonaldetails;

    @FindBy(how = How.XPATH, using ="//*[@id=\"sidenav\"]/li[6]/a")
    private WebElement btnJob;

    @FindBy(how = How.XPATH, using ="//*[@id=\"btnSave\"]")
    private WebElement btnEdit;

    @FindBy(how = How.XPATH, using ="//*[@id=\"job_job_title\"]/option[2]")
    private WebElement btnjobtitle;

    @FindBy(how = How.XPATH, using ="//*[@id=\"job_emp_status\"]/option[3]")
    private WebElement btnemploymentstatus;

    @FindBy(how = How.XPATH, using ="//*[@id=\"job_eeo_category\"]/option[2]")
    private WebElement btnjobcategory;

    @FindBy(how = How.XPATH, using ="//*[@id=\"job_joined_date\"]")
    private WebElement btnjoineddate;

    @FindBy(how = How.XPATH, using ="//*[@id=\"job_sub_unit\"]/option[5]")
    private WebElement btnsubunit;

    @FindBy(how = How.XPATH, using ="//*[@id=\"job_location\"]/option[3]")
    private WebElement btnlocation;

    @FindBy(how = How.XPATH, using ="//*[@id=\"job_contract_start_date\"]")
    private WebElement btnstartdate;

    @FindBy(how = How.XPATH, using ="//*[@id=\"btnSave\"]")
    private WebElement btnSave;

    @FindBy(how = How.XPATH, using ="//*[@id=\"searchBtn\"]")
    private WebElement btnSearch;

    @FindBy(how = How.XPATH, using ="//*[@id=\"resultTable\"]/tbody/tr/td[2]/a")
    private WebElement verDetalle;

    @FindBy(how = How.XPATH, using ="//*[@id=\"sidenav\"]/li[9]/a")
    private WebElement report;

    @FindBy(how = How.XPATH, using ="//*[@id=\"btnAddSupervisorDetail\"]")
    private WebElement btnAdd;

    @FindBy(how = How.XPATH, using ="//*[@id=\"reportto_supervisorName_empName\"]")
    private WebElement btnName;

    @FindBy(how = How.XPATH, using ="//*[@id=\"reportto_reportingMethodType\"]/option[2]")
    private WebElement btnReporting;

    @FindBy(how = How.XPATH, using ="//*[@id=\"btnSaveReportTo\"]")
    private WebElement btnSaveRep;

    @FindBy(how = How.XPATH, using ="//*[@id=\"resultTable\"]/tbody/tr/td[8]")
    private WebElement inputsupervisor;

    @FindBy(how = How.NAME, using ="chkSelectRow[]")
    private WebElement inputcheckbox;

    @FindBy(how = How.XPATH, using ="//*[@id=\"btnDelete\"]")
    private WebElement btnDelete;

    @FindBy(how = How.XPATH, using ="//*[@id=\"dialogDeleteBtn\"]")
    private WebElement btnOkEliminar;




    public void buscarempleado(String firstname, String middlename, String lastname){

        log.debug("buscando empleado");

        waitForElementToBeVisible(this.buscarempleado, this.wait);
        log.debug("ingresando nombre");
        this.buscarempleado.clear();
        this.buscarempleado.sendKeys(firstname +" "+ middlename +" "+ lastname);
        waitForElementToBeVisibleAndClickable(this.btnSearch, this.wait);
        this.btnSearch.click();
        log.info("Busqueda realizada");


    }
    public void verdetalle(){
        waitForElementToBeVisibleAndClickable(this.verDetalle, this.wait);
        this.verDetalle.click();
        log.info("empleado encontrado");

    }


    public void modificarDetalles(String joinedDate) {

       log.debug("modificar datos empleado");
        waitForElementToBeVisible(this.btnJob, this.wait);
        this.btnJob.click();

        log.debug("selecciono editar");
        waitForElementToBeVisible(this.btnEdit, this.wait);
        this.btnEdit.click();
        waitForElementToBeVisible(this.btnjobtitle, this.wait);
        this.btnjobtitle.click();
        waitForElementToBeVisible(this.btnemploymentstatus, this.wait);
        this.btnemploymentstatus.click();
        waitForElementToBeVisible(this.btnjobcategory, this.wait);
        this.btnjobcategory.click();
        log.debug("Ingresando datos");
        waitForElementToBeVisible(this.btnjoineddate, this.wait);
        this.btnjoineddate.clear();
        this.btnjoineddate.sendKeys(joinedDate);
        waitForElementToBeVisible(this.btnsubunit, this.wait);
        this.btnsubunit.click();
        waitForElementToBeVisible(this.btnlocation, this.wait);
        this.btnlocation.click();
        waitForElementToBeVisible(this.btnstartdate, this.wait);
        this.btnstartdate.clear();
        this.btnstartdate.sendKeys(joinedDate);
        log.debug("datos ingresado");

        waitForElementToBeVisible(this.btnSave, this.wait);
        this.btnSave.click();
        log.info("Datos guardados correctamente");

    }
    public void asignarsupervisor(String namesuperv) {

        log.debug("crear supervisor");
        waitForElementToBeVisible(this.report, this.wait);
        this.report.click();
        waitForElementToBeVisibleAndClickable(this.btnAdd, this.wait);
        this.btnAdd.click();
        log.debug("creando supervisor");
        waitForElementToBeVisible(this.btnName, this.wait);
        this.btnName.sendKeys(namesuperv);
        waitForElementToBeVisible(this.btnReporting, this.wait);
        this.btnReporting.click();
        waitForElementToBeVisible(this.btnSaveRep, this.wait);
        this.btnSaveRep.click();
        log.info("Supervisor creado");
        this.menuemployee.click();

    }


    public void eliminarempleado(){

        log.debug("Selecciono empleado");
        waitForElementToBeVisible(this.inputcheckbox, this.wait);
        this.inputcheckbox.click();
        log.debug("Eliminando empleado");
        waitForElementToBeVisible(this.btnDelete, this.wait);
        this.btnDelete.click();
        waitForElementToBeVisibleAndClickable(this.btnOkEliminar, this.wait);
        this.btnOkEliminar.click();
        log.info("Empleado eliminado");


    }



}
