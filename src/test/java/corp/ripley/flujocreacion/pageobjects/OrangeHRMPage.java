package corp.ripley.flujocreacion.pageobjects;

import corp.ripley.flujocreacion.utils.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static corp.ripley.flujocreacion.utils.MetodosGenericos.waitForElementToBeVisible;
import static corp.ripley.flujocreacion.utils.MetodosGenericos.waitForElementToBeVisibleAndClickable;

public class OrangeHRMPage extends BasePage {
    //Se utilizará Selenium

    //Aca se colocan: Los elementos de la pagina y las acciones

    //Elementos Web
    @FindBy(how = How.XPATH, using ="//*[@id=\"txtUsername\"]")
    private WebElement userName;

    @FindBy(how = How.XPATH, using ="//*[@id=\"txtPassword\"]")
    private WebElement password;

    @FindBy(how = How.XPATH, using ="//*[@id=\"btnLogin\"]")
    private WebElement btnLogin;

    @FindBy(how = How.XPATH, using ="//*[@id=\"welcome\"]")
    private WebElement btnWelcome;

    @FindBy(how = How.XPATH, using ="//*[@id=\"welcome-menu\"]/ul/li[3]")
    private WebElement btnLogout;





    //Acciones
    public void ingresarAOrange(String url, String userName, String password){
        this.driver.get(url);
        log.debug("Navegación hacia la url: " + url);
        log.debug("Verifiando que los elementos del login sean visibles");
        waitForElementToBeVisible(this.userName, this.wait);
        waitForElementToBeVisible(this.password, this.wait);
        waitForElementToBeVisibleAndClickable(this.btnLogin, this.wait);

        log.debug("Ingresando credenciales");
        this.userName.sendKeys(userName);
        this.password.sendKeys(password);
        this.btnLogin.click();
        log.debug("Verificando que pagina web cargue correctamente");



    }

    public void desloguear(){
        log.debug("Cerrar sesion");
        waitForElementToBeVisible(this.btnWelcome, this.wait);
        this.btnWelcome.click();
        waitForElementToBeVisible(this.btnLogout,this.wait);
        this.btnLogout.click();
        log.info("Sesion cerrada");

    }







}
