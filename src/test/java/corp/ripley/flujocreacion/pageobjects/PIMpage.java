package corp.ripley.flujocreacion.pageobjects;

import corp.ripley.flujocreacion.utils.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static corp.ripley.flujocreacion.utils.MetodosGenericos.waitForElementToBeVisible;
import static corp.ripley.flujocreacion.utils.MetodosGenericos.waitForElementToBeVisibleAndClickable;

public class PIMpage extends BasePage {

    //Elementos Web

    @FindBy(how = How.XPATH, using ="//*[@id=\"menu_pim_viewPimModule\"]")
    private WebElement menuPIM;

    @FindBy(how = How.XPATH, using ="//*[@id=\"menu_pim_addEmployee\"]")
    private WebElement menuADD;

    @FindBy(how = How.XPATH, using ="//*[@id=\"firstName\"]")
    private WebElement firstname;

    @FindBy(how = How.XPATH, using ="//*[@id=\"middleName\"]")
    private WebElement middlename;

    @FindBy(how = How.XPATH, using ="//*[@id=\"lastName\"]")
    private WebElement lastname;

    @FindBy(how = How.XPATH, using ="//*[@id=\"employeeId\"]")
    private WebElement idEmployee;

    @FindBy(how = How.XPATH, using ="//*[@id=\"btnSave\"]")
    private WebElement btnSave;


    @FindBy(how = How.XPATH, using ="//*[@id=\"personal_txtEmpFirstName\"]")
    private WebElement resultadofirstname;
    @FindBy(how = How.XPATH, using ="//*[@id=\"personal_txtEmpMiddleName\"]")
    private WebElement resultadomiddlename;
    @FindBy(how = How.XPATH, using ="//*[@id=\"personal_txtEmpLastName\"]")
    private WebElement resultadolastname;
    @FindBy(how = How.XPATH, using ="//*[@id=\"personal_txtEmployeeId\"]")
    private WebElement resultadoemployeeid;

    @FindBy(how = How.CSS, using ="personalDetails")
    private WebElement contenedordetalles;

    @FindBy(how = How.XPATH, using ="//*[@id=\"sidenav\"]/li[1]/a")
    private WebElement btnpersonaldetails;




    //Acciones

    public void homeOrangeHRM(){
        waitForElementToBeVisible(this.menuPIM, this.wait);
        log.info("Se a logeado correctamente");
        this.menuPIM.click();

        log.debug("Selecciono menu");

            }
    public void agregarUsuario(String firstname, String middlename, String lastname, String idEmployee){
        waitForElementToBeVisible(this.menuADD, this.wait);
        log.debug("ingreso a menu ADD");
        waitForElementToBeVisible(this.menuADD, this.wait);
        this.menuADD.click();

        log.debug("Ingresando datos de empleado");
        waitForElementToBeVisible(this.firstname, this.wait);
        waitForElementToBeVisible(this.middlename, this.wait);
        waitForElementToBeVisible(this.lastname, this.wait);
        waitForElementToBeVisible(this.idEmployee, this.wait);
        this.firstname.sendKeys(firstname);
        this.middlename.sendKeys(middlename);
        this.lastname.sendKeys(lastname);
        log.info("Datos ingresados");
        log.debug("limpiando campo idemployee");
        this.idEmployee.clear();
        this.idEmployee.sendKeys(idEmployee);
        log.debug("campo idemployee agregado");
        waitForElementToBeVisibleAndClickable(this.btnSave, this.wait);
        this.btnSave.click();
        log.info("Empleado creado correctamente");

    }
    public void verificarDatosEmpleado(){
        log.debug("Verificando datos ingrasados");

        this.btnpersonaldetails.click();
        wait.until(ExpectedConditions.visibilityOf(resultadofirstname));
        wait.until(ExpectedConditions.visibilityOf(resultadomiddlename));
        wait.until(ExpectedConditions.visibilityOf(resultadolastname));
        wait.until(ExpectedConditions.visibilityOf(resultadoemployeeid));
        log.info("datos corroborados");

    }





        }




