package corp.ripley.flujocreacion.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    //Para logs:
    protected static final Logger log = LogManager.getLogger();

    private static final int TIMEOUT = 20;
    private static final int POLLING = 20;

    public BasePage(){
        this.driver = corp.ripley.flujocreacion.utils.WebDriverFactory.getDriver();
        wait = new WebDriverWait(this.driver, Duration.ofSeconds(TIMEOUT), Duration.ofMillis(POLLING));
        PageFactory.initElements(new AjaxElementLocatorFactory(corp.ripley.flujocreacion.utils.WebDriverFactory.getDriver(), TIMEOUT), this);
    }
}
