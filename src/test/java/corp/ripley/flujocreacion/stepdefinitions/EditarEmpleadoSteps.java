package corp.ripley.flujocreacion.stepdefinitions;

import corp.ripley.flujocreacion.pageobjects.EmployeeListPage;
import corp.ripley.flujocreacion.pageobjects.OrangeHRMPage;
import corp.ripley.flujocreacion.pageobjects.PIMpage;
import corp.ripley.flujocreacion.utils.MetodosGenericos;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.io.IOException;
import java.util.Properties;

public class EditarEmpleadoSteps {


    //Se le pone como arugmento la ruta donde estarán los properties
    Properties prop = MetodosGenericos.determinarPropertiesPorAmbiente("src/test/resources/properties");

    private final OrangeHRMPage orangeHRMPage;
    private final PIMpage piMpage;
    private final EmployeeListPage employeeListPage;


    //Constructor
    public EditarEmpleadoSteps(OrangeHRMPage orangeHRMPage, PIMpage piMpage, EmployeeListPage employeeListPage) throws IOException {

        this.orangeHRMPage = orangeHRMPage;
        this.piMpage = piMpage;
        this.employeeListPage = employeeListPage;
    }

    @Given("Edito datos del empleado")
    public void editoDatosDelEmpleado() {

        orangeHRMPage.ingresarAOrange(
                prop.getProperty("url"),
                prop.getProperty("userName"),
                prop.getProperty("password"));
        piMpage.homeOrangeHRM();
        employeeListPage.buscarempleado(
                prop.getProperty("firstname"),
                prop.getProperty("middlename"),
                prop.getProperty("lastname"));
        employeeListPage.verdetalle();
        employeeListPage.modificarDetalles(
                prop.getProperty("joinedDate"));
        employeeListPage.asignarsupervisor(
                prop.getProperty("namesuperv"));
    }

    @Then("Se edita empleado correctamente")
    public void seEditaEmpleadoCorrectamente() {

            employeeListPage.buscarempleado(
                    prop.getProperty("firstname"),
                    prop.getProperty("middlename"),
                    prop.getProperty("lastname"));

        }



}