package corp.ripley.flujocreacion.stepdefinitions;

import corp.ripley.flujocreacion.pageobjects.OrangeHRMPage;
import corp.ripley.flujocreacion.pageobjects.PIMpage;
import corp.ripley.flujocreacion.utils.MetodosGenericos;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.io.IOException;
import java.util.Properties;

public class CreoEmpleadoSteps {


    //Se le pone como arugmento la ruta donde estarán los properties
    Properties prop = MetodosGenericos.determinarPropertiesPorAmbiente("src/test/resources/properties");

    private final OrangeHRMPage orangeHRMPage;
    private final PIMpage piMpage;


    //Constructor
    public CreoEmpleadoSteps(OrangeHRMPage orangeHRMPage, PIMpage piMpage) throws IOException {

        this.orangeHRMPage = orangeHRMPage;
        this.piMpage = piMpage;

    }

    @Given("Estoy en la pagina principal de Orange")
    public void estoyEnLaPaginaPrincipalDeOrange() {

        orangeHRMPage.ingresarAOrange(
                prop.getProperty("url"),
                prop.getProperty("userName"),
                prop.getProperty("password"));
        piMpage.homeOrangeHRM();
        piMpage.agregarUsuario(
                prop.getProperty("firstname"),
                prop.getProperty("middlename"),
                prop.getProperty("lastname"),
                prop.getProperty("idEmployee"));
    }

    @Then("Se agrega empleado correctamente")
    public void seAgregaEmpleadoCorrectamente() {
            piMpage.verificarDatosEmpleado();

        }
}





