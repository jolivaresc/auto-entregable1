package corp.ripley.flujocreacion.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

import static io.cucumber.testng.CucumberOptions.SnippetType.CAMELCASE;

@CucumberOptions(
        //Aca se coloca ruta de los archivos .feature
        features = {"src/test/resources/features"},
        //Aca se coloca la ruta de las clases que implementan los pasos de .feature
        glue = {"corp/ripley/flujocreacion/stepdefinitions"},
        tags = " ",
        snippets = CAMELCASE,
        plugin = {"summary","pretty",
                "json:target/cucumber/report.json",
                "html:target/cucumber/report.html"},
        monochrome=false,
        dryRun=false
)



public class RunCrear extends AbstractTestNGCucumberTests {
    //Hacemos un override del método scenarios que es dataprovider
    @Override
    @DataProvider(parallel = false) //Para activar el modo paralelo
    public Object[][] scenarios() {
        return super.scenarios();
    }

}
